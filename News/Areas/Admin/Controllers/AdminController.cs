﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using News.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace News.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AdminController : Controller
    {
        // GET: AdminController
        NewContext db;
        public AdminController(NewContext context)
        {
            db = context;
        }
        public ActionResult Index()
        {
            int TC = db.Teams.Count();
            int CC = db.Categories.Count();
            int NC = db.News.Count();
            int COC = db.Contacts.Count();
            var result = new AdminNumbers
            {
                team = TC,
                cat = CC,
                news = NC,
                coment = COC,

            };
            return View(result);
        }
        public ActionResult Cleander()
        {
            return View();
        }
        public ActionResult Compose()
        {
            return View();
        }

        public ActionResult Read()
        {
            return View();
        }
    }
    public class AdminNumbers
    {
        public int team { get; set; }
        public int cat { get; set; }
        public int news { get; set; }
        public int coment  { get; set; }
    }
}
