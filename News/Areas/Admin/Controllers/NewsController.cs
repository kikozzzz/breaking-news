﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using News.Models;

namespace News.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class NewsController : Controller
    {
        private readonly NewContext _context;
        private IHostingEnvironment host;

        public NewsController(NewContext context, IHostingEnvironment hosting)
        {
            _context = context;
            host = hosting;
        }

        // GET: Admin/News
        public async Task<IActionResult> Index()
        {
            var newContext = _context.News.Include(z => z.Category);
            return View(await newContext.ToListAsync());
        }

        // GET: Admin/News/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @new = await _context.News
                .Include(z => z.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@new == null)
            {
                return NotFound();
            }

            return View(@new);
        }

        // GET: Admin/News/Create
        public IActionResult Create()
        {
            ViewData["CategoryID"] = new SelectList(_context.Categories, "Id", "Name");
            return View();
        }

        // POST: Admin/News/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( New @new)
        {
            if (ModelState.IsValid)
            {
                UploadPhoto(@new);
                _context.Add(@new);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "Id", "Name", @new.CategoryID);
            return View(@new);
        }

        void UploadPhoto(New model)
        {
            if (model.File != null)
            {
                string uploadsFolder = Path.Combine(host.WebRootPath, "Manager/Imgs/News");
                string uniqueFileName = new Guid() + ".jpg";
                string FilePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var filestream = new FileStream(FilePath, FileMode.Create))
                {
                    model.File.CopyTo(filestream);
                }
                model.Image = uniqueFileName;
            }
        }

        // GET: Admin/News/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @new = await _context.News.FindAsync(id);
            if (@new == null)
            {
                return NotFound();
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "Id", "Name", @new.CategoryID);
            return View(@new);
        }

        // POST: Admin/News/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, New @new)
        {
            if (id != @new.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    UploadPhotoChange(@new);
                    _context.Update(@new);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NewExists(@new.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.Categories, "Id", "Name", @new.CategoryID);
            return View(@new);
        }

        void UploadPhotoChange(New model)
        {
            if (model.File != null)
            {
                string uploadsFolder = Path.Combine(host.WebRootPath, "Manager/Imgs/News");
                string uniqueFileName = Guid.NewGuid() + "jpg";
                string FilePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var filestream = new FileStream(FilePath, FileMode.Create))
                {
                    model.File.CopyTo(filestream);
                }
                model.Image = uniqueFileName;
            }
        }



        // GET: Admin/News/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @new = await _context.News
                .Include(z => z.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@new == null)
            {
                return NotFound();
            }

            return View(@new);
        }

        // POST: Admin/News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @new = await _context.News.FindAsync(id);
            _context.News.Remove(@new);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NewExists(int id)
        {
            return _context.News.Any(e => e.Id == id);
        }
    }
}
