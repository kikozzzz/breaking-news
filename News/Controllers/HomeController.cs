﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using News.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace News.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly ILogger<HomeController> _logger;
        NewContext db;
        public HomeController (NewContext context, ILogger<HomeController> logger)
        {
            db = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var Result=db.Categories.ToList();
            return View(Result);
        }

        //[Authorize]
        public IActionResult News(int id)
        {
            Category Z = db.Categories.Find(id);
            //ViewBag.X = Z.Name;
            ViewData["Z"] = Z.Name;
            var Loka = db.News.Where(y => y.CategoryID == id).OrderByDescending(x=>x.Date).ToList();
            return View(Loka);
        }

       
        public IActionResult DeleteNews(int id)
        {
            var M = db.News.Find(id);
            db.News.Remove(M);
            db.SaveChanges();
            return RedirectToAction("News");

        }

        public IActionResult ContactUS()
        {
            return View();
            
        }

        public IActionResult Comentes()
        {
            
            return View(db.Contacts.ToList());
        }

        [HttpPost]
        public IActionResult SendContect(Contact model)
        {
            if (ModelState.IsValid)
            {
                db.Contacts.Add(model);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View("ContactUS",model);
           
        }
        public IActionResult About()
        {
            var Team = db.Teams.ToList();
            return View(Team);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
