﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace News.Models
{
    public class New
    {
        public int Id { get; set; }
        public string Titel { get; set; }
        public string Image { get; set; }
        public string Topic { get; set; }
        public DateTime Date { get; set; }
        [ForeignKey("Category")]
        public int CategoryID { get; set; }
        public Category Category { get; set; }

        [NotMapped]
        public IFormFile File { get; set; }
    }
}
