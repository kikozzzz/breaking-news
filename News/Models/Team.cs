﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace News.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string JobTitle { get;set; }
        public string Image { get; set; }

        [NotMapped]
        public IFormFile file { get; set; }

    }
}
